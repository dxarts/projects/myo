
Myo {
	var langPort, <quatData, <accelData, <gyroData, <linear_accelData, <>server, startMyo, multiMyos;
	var <pid, <quaternion, <emg, <gesture, <accel, <gyro, <linear_accel, <connectedBands, <>connectedFuncs;
	var <indices, cond, <>verbose = false;
	var <cmdPath, anotherMyo = false;
	var shutdown;
	var <myoReceive;
	classvar <>myoAppPath;

	*initClass {
		myoAppPath = File.realpath(this.filenameSymbol).asPathName.parentPath;
		myoAppPath = File.realpath(myoAppPath ++ "/../Myo_Relay/myo-osc-relay");
	}

	*new { |langPort, quatData = true, accelData = false, gyroData = false, linear_accelData = false, server, startMyo = true, multiMyos = false|
		^super.newCopyArgs(langPort, quatData, accelData, gyroData, linear_accelData, server, startMyo, multiMyos).init
	}

	init {
		cond = Condition.new;
		// boot the server
		server = server ?? {Server.default.boot};
		{
			// check if another myo-osc-relay is running (OS X only?)
			"ps axo pid,command | grep \"[m]yo-osc-relay\"".unixCmd{|err|
				anotherMyo = err.asBoolean.not;
				cond.test_(true).signal
			};
			cond.wait;

			// only if there is not another myo-osc-relay, or user wants multiple running, proceed
			(anotherMyo and: multiMyos.not).if({
				"Another Myo is currently runing. Not proceeding".warn;
			}, {
				langPort = langPort ?? { NetAddr.langPort };
				emg = IdentityDictionary.new(know: true);
				gesture = IdentityDictionary.new(know: true);
				indices = IdentityDictionary.new(know: true);
				connectedBands = IdentityDictionary.new(know: true);
				connectedFuncs = IdentityDictionary.new(know: true);
				indices.emg = (6..13);
				quatData.if({
					quaternion = IdentityDictionary.new(know: true);
					indices.quat = [17, 14, 15, 16]
				});
				accelData.if({
					accel = IdentityDictionary.new(know: true);
					indices.accel = indices.asArray.flatten.maxItem + [1, 2, 3]
				});
				gyroData.if({
					gyro = IdentityDictionary.new(know: true);
					indices.gyro = indices.asArray.flatten.maxItem + [1, 2, 3]
				});
				linear_accelData.if({
					linear_accel = IdentityDictionary.new(know: true);
					indices.linear_accel = indices.asArray.flatten.maxItem + [1, 2, 3]
				});
				startMyo.if({ this.myoOSCRelay });

				this.receiveOSC;
				shutdown = { this.cleanup };
				ShutDown.add(shutdown);
				CmdPeriod.doOnce({ this.cleanup })
			})
		}.forkIfNeeded


	}

	myoOSCRelay {
		cmdPath = Myo.myoAppPath.escapeChar($ ) + "-p" + langPort + "-emg";
		quatData.if(
			{cmdPath = cmdPath + "-quat"}
		);
		accelData.if(
			{cmdPath = cmdPath + "-accel"}
		);
		gyroData.if(
			{cmdPath = cmdPath + "-gyro"}
		);
		linear_accelData.if(
			{cmdPath = cmdPath + "-linaccel"}
		);
		pid = cmdPath.unixCmd;
	}

	receiveOSC {
		// OSCdef('myoReceive').func.isNil.if({
		myoReceive = OSCFunc({ |msg|
			var band;
			// get MAC Address as Symbol
			band = msg[2].asSymbol;
			verbose.if({
				msg.postln;
			});
			emg.keys.includes(band).not.if({
				emg.add(band -> {});
				quatData.if({ quaternion.add(band -> Quaternion.new) });
				accelData.if({ accel.add(band -> Cartesian.new) });
				gyroData.if({ gyro.add(band -> Cartesian.new) });
				linear_accelData.if({ linear_accel.add(band -> Cartesian.new) })
			});
			// if the band connection status has changed
			// (msg[3] != connectedBands[band]).if({
			// 	connectedFuncs[band].value(msg[3])
			// });

			emg[band].value(msg[indices.emg]);
			quatData.if({ quaternion[band].set(*msg[indices.quat]) });
			accelData.if({ accel[band].set(*msg[indices.accel]) });
			gyroData.if({ gyro[band].set(*msg[indices.gyro]) });
			linear_accelData.if({ linear_accel[band].set(*msg[indices.linear_accel]) });
			gesture[band] = msg[5];

			connectedBands[band] = true;
		}, '/myo', recvPort: langPort)
	}

	cleanup {
		emg.do(_.free);
		myoReceive.free;
		startMyo.if({ ("kill -9" + pid).unixCmd;
			"killall -9 myo-osc-relay".unixCmd });
		"Myo main class has been cleared!".postln;
		ShutDown.remove(shutdown)
	}

}

