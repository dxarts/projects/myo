MyoWekinator {
	var myo, macAddress, sendPort, receivePort, <receiveMessages, quatData;
	var wekAddress, replyMessage;
	var wekInputOSC, wekOutputOSC;
	var <receiveMsg;

	*new {arg myo, macAddress, sendPort, receivePort, receiveMessages, quatData = false;
		^super.newCopyArgs(myo, macAddress, sendPort, receivePort, receiveMessages, quatData).init
	}

	init {
		replyMessage = ('/reply' ++ macAddress).asSymbol;
		wekAddress = NetAddr.new("localhost", sendPort);
		this.oscReply;
		this.oscWek;
		this.startWek;
		ShutDown.add({this.cleanup})
	}

	oscReply {
		wekInputOSC = OSCFunc({arg msg;
			var wekMsg;
			quatData.if({wekMsg = msg[3..10] ++ myo.quaternion[macAddress].toArray}, {wekMsg = msg[3..10]});
			wekAddress.sendMsg('/wek/inputs', *wekMsg)
		}, replyMessage)
	}

	oscWek {
		wekOutputOSC = receiveMessages.collect{arg message, i;
			OSCFunc({arg msg;
				receiveMsg = msg;
			}, message, recvPort: receivePort)
		};
	}

	startWek {
		wekAddress.sendMsg('/wekinator/control/startRunning')
	}

	stopWek {
		wekAddress.sendMsg('/wekinator/control/stopRunning')
	}

	cleanup {
		this.stopWek;
		wekInputOSC.do{arg oscFunc;
			oscFunc.free
		};
		wekInputOSC.free;
	}
}

MyoWekinatorMultiBand {
	var myo, macAddresses, sendPort, receivePort, receiveMessages, quatData;
	var wekAddress, replyMessages;
	var replyDefs, wekDefs;
	var <receiveMsg;
	var wekMsg, wekSendTask;

	*new {arg myo, macAddresses, sendPort, receivePort, receiveMessages, quatData = false;
		^super.newCopyArgs(myo, macAddresses, sendPort, receivePort, receiveMessages, quatData).init
	}

	init {
		wekMsg = Array.newClear(macAddresses.size);
		replyMessages = macAddresses.collect{arg address;
			('/reply' ++ address).asSymbol
		};
		wekAddress = NetAddr.new("localhost", sendPort);
		this.oscReply;
		this.oscWek;
		this.startWek;
		wekSendTask = this.oscSend.play;
	}

	oscReply {
		replyDefs = replyMessages.collect{arg replyMessage, i;
			OSCFunc({arg msg;
				quatData.if({wekMsg[i] = msg[3..10] ++ myo.quaternion[macAddresses[i]].toArray}, {wekMsg[i] = msg[3..10]});
			}, replyMessage)
		};
	}

	oscSend {
		^Task({
			loop{
				wekAddress.sendMsg('/wek/inputs', *wekMsg.flatten);
				0.02.wait
			}
		})
	}

	oscWek {
		wekDefs = receiveMessages.collect{arg message, i;
			OSCFunc({arg msg;
				receiveMsg = msg;
			}, message, recvPort: receivePort)
		};
	}

	startWek {
		wekAddress.sendMsg('/wekinator/control/startRunning')
	}

	stopWek {
		wekAddress.sendMsg('/wekinator/control/stopRunning')
	}

	cleanup {
		this.stopWek;
		wekSendTask.stop;
		wekDefs.do(_.free);
		replyDefs.do(_.free);
	}
}