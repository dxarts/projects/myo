MyoBand {
	var myo, <macAddress, filterGraph, trigRate;
	var <synth, <emg, <quat, <gesture, <accel, <gyro, <linear_accel, replyMessage;
	var <rtt, synthDef, <>calibrateRotation, <>calibrateTilt,  calibrateRotation, calibrateTumble, <bandConnected, connectionFunc, <oscFunc;
	var <>verbose = false, <>calInst = false, <>emgThresh, <emgInbus, <emgOutbus;
	var server, shutdown;

	*new { |myo, macAddress, filterGraph, trigRate = 20|
		^super.newCopyArgs(myo, macAddress, filterGraph, trigRate).init
	}

	init {

		{
			server = myo.server;
			emgOutbus = Bus.control(server, 8);
			emgInbus = Bus.control(server, 8);
			myo.emg[macAddress] = {|vals| emgInbus.set(*vals)};
			replyMessage = ('/reply' ++ macAddress).asSymbol;
			calibrateRotation = 0.0;
			calibrateTilt = 0.0;
			calibrateTumble = 0.0;
			rtt = IdentityDictionary.new(know: true);
			rtt.putPairs(['rotate',  nil, 'tilt', nil, 'tumble', nil]);
			emgThresh = IdentityDictionary.new(know: true);
			emgThresh.putPairs(['min', 0, 'max', 1]);
			this.oscReply;
			this.initiateSynth;
			shutdown = { this.cleanup };
			ShutDown.add(shutdown);
			CmdPeriod.doOnce({ this.cleanup(false) })
		}.forkIfNeeded
	}

	initiateSynth {
		synthDef = this.filterSynth;
		synthDef.add;
		myo.server.sync;
		synth = Synth(synthDef.name, [\inbus, emgInbus.asMap, \outbus, emgOutbus.asMap, \trigRate, trigRate]);
		myo.server.sync;
	}

	filterSynth {
		^SynthDef.new(macAddress, {arg inbus, outbus, trigRate;
			var data, filter, trigger, numsamp;
			numsamp = 20;
			data = In.kr(inbus, 8);
			trigger = Impulse.kr(trigRate);
			filter = filterGraph.isNil.not.if({
				filterGraph.value(data)
			}, {
				(RunningSum.kr(data.squared,numsamp)/numsamp).sqrt
			});
			Out.kr(outbus, filter);
			SendReply.kr(trigger, replyMessage, filter)
		})
	}

	oscReply {
		oscFunc = OSCFunc({ |msg|
			verbose.if({
				msg.postln
			});
			emg = msg[3..10];
			gesture = myo.gesture[macAddress];
			myo.accelData.if({ accel = myo.accel[macAddress] });
			myo.gyroData.if({ gyro = myo.gyro[macAddress] });
			myo.linear_accelData.if({ linear_accel = myo.linear_accel[macAddress] });
			myo.quatData.if({
				quat = myo.quaternion[macAddress];
				quat.notNil.if({
					// calInst.if({ this.calibrate });
					rtt.rotate = (quat.rotate + calibrateRotation).mod(2pi).linlin(0.0, 2pi, -pi, pi);
					rtt.tilt = (quat.tilt - calibrateTilt).wrap(-pi, pi);
					rtt.tumble = (quat.tumble - calibrateTumble).wrap(-pi, pi);
				})
			});
		}, replyMessage, myo.server.addr)
	}

	calibrate { |rotate = true, tilt = false, tumble = false|
		quat.notNil.if({
			rotate.if({ calibrateRotation = pi - quat.rotate });
			tilt.if({ calibrateTilt = quat.tilt });
			tumble.if({ calibrateTumble = quat.tumble })
		}, {
			("Could not calibrate. No quaterion data for band" + macAddress).warn;
		})
	}

	calibrateEmgMax {
		emg.notNil.if({
			emgThresh.max_(emg.sum/8);
			^emg.sum/8
		}, {
			"Could not set emg max. No emg data".warn;
		})
	}

	calibrateEmgMin {
		emg.notNil.if({
			emgThresh.min_(emg.sum/8);
			^emg.sum/8
		}, {
			"Could not set emg min. No emg data".warn;
		})
	}

	emgMax_ { |max|
		emg.notNil.if({
			emgThresh.max_(max)
		})
	}

	emgMin_ { |min|
		emg.notNil.if({
			emgThresh.min_(min)
		})
	}

	emgNormSum { |min = 0, max = 1, default = 0|
		emg.notNil.if({
			^(emg.sum/8).linlin(emgThresh.min, emgThresh.max, min, max)
		}, {
			"Could not get emg normalized sum. No emg data".warn;
			^default
		})
	}

	cleanup { |freeSynth = true|
		freeSynth.if({ synth.free });
		emgOutbus.free;
		emgInbus.free;
		ShutDown.remove(shutdown);
		oscFunc.free;
		("Myo band " ++ macAddress ++ " has been cleared!").postln;
	}


}